<?php
/*********************************************************************
 * This class provides a PHP wrapper used in combination with the  
 * MFS Africa Card API Java package to make API requests and format responses 
 * for use in a PHP environment.
 *
 * @author Aloysious Zziwa <azziwa@olycash.com>
 * @version 1.0.0
 * @created 05/15/2022
 * @copyright (c) 2022 OlyCash Inc. and its affiliates. 
 *
 * Free permission to use and reproduce granted under MIT license. 
 *********************************************************************/
require_once('config.php'); 


class Wrapper {
    
    # Decrypt given response string
    function decrypt($parameters) {
        # Ensure we are dealing with a string
        if(!is_string($parameters['data'])) {
            try{
                $parameters['data'] = (string)$parameters['data'];
            } catch (Exception $e) {
                return '';
            }
        }
        
        $result = $this->run_java(['direction'=>'decrypt', 'data'=>$parameters['data'] ]);
        if(!empty($result['message']) && $result['message'] == 'success') {
            // attempt to return as extracted array
            try{
                return $this->inner_json_decode($result['details']);
            } catch (Exception $e) {
                # failed to extract JSON, just return as is
                return $result['details'];
            }
            
        }
        else return '';
    }
    
    
    
    # Encrypt given data string
    function encrypt($parameters) {
        # Ensure we are dealing with a string
        if(!is_string($parameters['data'])) {
            try{
                $parameters['data'] = json_encode($parameters['data']);
            } catch (Exception $e) {
                return '';
            }
        }
        
        $result = $this->run_java(['direction'=>'encrypt', 'data'=>$parameters['data'] ]);
        if(!empty($result['message']) && $result['message'] == 'success') return $result['details'];
        else return '';
    }
    
    
    
    
    # Run command the requires access to Java library
    function run_java($command){
        # Ignore command with invalid direction
        if(!in_array($command['direction'], ['encrypt','decrypt'])) return ["message"=>"error", "details"=>'invalid_value__direction'];
        if(empty($command['data'])) return ["message"=>"error", "details"=>'invalid_value__data'];
        
        $dependencies = unserialize(JAVA_DEPENDENCIES);
        # prepare command string to include dependencies to run with processor
        # java -cp ".:/path/to/lib/dependency1.jar:/path/to/lib/dependency2.jar:../" mfs.Processor [direction] [shared secret] [string for command/response]
        $commandString = 'java -cp ".';
        foreach($dependencies AS $libraryFile) $commandString .= ':'.__DIR__.'/lib/'.$libraryFile;
        $commandString .= ':'.__DIR__.'/lib/" mfs.Processor '.$command['direction'].' "'.SHARED_SECRET.'" "'.$command['data'].'"';
        
        # WARNING: This function is vulnerable to attack!
        # Ensure that the command string passed is verified before it reaches
        # this stage or use on a separate expendable server.
        #
        # If the $response is not being received, activate by removing from the
        # "disable_functions" list of functions in your php.ini
        # and your php user (e.g. www-data) can run the Java Request command.
        $response = shell_exec($commandString);
        #echo "RAW RESPONSE: ".json_encode($response);
        
        # $response in the format
        # [warning text]
        # ::RESPONSE:: [desired value] <== success!
        # ::ERROR:: [desired value] <== failed
        
        if(strpos($response, '::RESPONSE:: ') !== FALSE){
            $result = explode('::RESPONSE:: ', $response)[1];
            return ["message"=>"success", "details"=>$result];
        }
        else if(strpos($response, '::ERROR:: ') !== FALSE){
            $result = explode('::ERROR:: ', $response)[1];
            return ["message"=>"fail", "details"=>$result];
        }
        else return ["message"=>"error", "details"=>'error'];
    }
	
    
    
    # First space out the json for proper interpretation
    function inner_json_decode($string) {
        $string = html_entity_decode($string, ENT_QUOTES);
        $string = preg_replace('/(\w+):/i', '"\1":', $string);
        return json_decode($string, true);
    }
}





