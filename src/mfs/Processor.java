/********************************************************************
 * This class processes commands on the MFS Africa Card API by:
 * a) Encrypting commands for requests
 * b) Decrypting reponses from API 
 *
 * @author Aloysious Zziwa <azziwa@olycash.com>
 * @version 1.0.0
 * @created 05/15/2022
 * @copyright (c) 2022 OlyCash Inc. and its affiliates. 
 *
 * Free permission to use and reproduce granted under MIT license. 
 ********************************************************************/
package mfs;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.spec.SecretKeySpec;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.lang.JoseException;



/****************************************************************************
* NOTE (if operating with dependencies): 
* To recompile: 
* $ javac -cp ".:/path/to/lib/jose4j-0.7.12.jar:/path/to/lib/slf4j-api-1.7.21.jar" Processor.java
*
* To run on command line:
* $ java -cp ".:/path/to/lib/jose4j-0.7.12.jar:/path/to/lib/slf4j-api-1.7.21.jar:../" mfs.Processor [direction] [shared secret] [string for command/response]
*
* Values for [direction] are encrypt, decrypt
******************************************************************************/ 
class Processor {

    public static void main (String args[]) 
        throws NoSuchAlgorithmException, IOException {
        
        // Ensure that the parameters passed are properly assigned
        HashMap<String, String> parameters = new HashMap<String, String>();
        int totalParameters = args.length;
        
        if(totalParameters > 2){
            // Extract the desired parameters from the arguments of the command
            // For now, ignore the rest of the argurments beyond the first three
            parameters.put("DIRECTION", args[0].toUpperCase());
            parameters.put("SHARED_SECRET", args[1]);
            parameters.put("COMMAND_TEXT", args[2]);
            
            // process based on direction
            if("ENCRYPT".equals(parameters.get("DIRECTION"))) {
                try {
                    String encryptedMessage = encrypt(parameters.get("SHARED_SECRET"), parameters.get("COMMAND_TEXT"));
                    System.out.println("::RESPONSE:: " + encryptedMessage);
                } catch(UnsupportedEncodingException | JoseException e){
                    System.out.println("::ERROR:: Problem encrypting message.");
                }
            }
            else if("DECRYPT".equals(parameters.get("DIRECTION"))) {
                try {
                    String decryptedMessage = decrypt(parameters.get("SHARED_SECRET"), parameters.get("COMMAND_TEXT"));
                    System.out.println("::RESPONSE:: " + decryptedMessage);
                } catch(UnsupportedEncodingException | JoseException e){
                    System.out.println("::ERROR:: Problem decrypting message.");
                }
            }
            // an error in the direction argument
            else {
                System.out.println("::ERROR:: Invalid encryption direction. See class documentation.");
            }
            
        } else {
            // not enough parameters were submitted
            System.out.println("::ERROR:: Invalid request details. See class documentation.");
        }
    }







    // Encrypt a string and return the encrypted version
    public static String encrypt(String sharedSecret, String commandText)
        throws NoSuchAlgorithmException, UnsupportedEncodingException, JoseException {

        // Prepare digest from the shared secret (obtain from MFS Beforehand)
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(sharedSecret.getBytes("UTF-8"));
        
        // Then prepare the encryption key from the digest
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A256GCMKW);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_256_GCM);
        jwe.setKey(new SecretKeySpec(digest, "AES"));
        
        // Encrypt the message
        jwe.setPayload(commandText);
        String encryptedMessage = jwe.getCompactSerialization();
        
        return encryptedMessage;
    }



    







    // Encrypt a string and return the encrypted version
    public static String decrypt(String sharedSecret, String responseText)
        throws NoSuchAlgorithmException, UnsupportedEncodingException, JoseException {

        // Prepare digest from the shared secret (obtain from MFS Beforehand)
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(sharedSecret.getBytes("UTF-8"));
        
        // Then prepare the encryption key from the digest
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setCompactSerialization(responseText);
        jwe.setKey(new SecretKeySpec(digest, "AES"));
        
        // Then decrypt the message
        String decryptedMessage = jwe.getPlaintextString();

        return decryptedMessage;
    }



}