# mfs-africa-card-api

ABOUT:
Use to run requests on the MFS Africa (https://mfsafrica.com) Card API whose documentation can be found at:

https://sandbox.mfsafricacards.com/MFSAfricaCard/swagger-ui.html?urls.primaryName=mfsafricacard

with either Java or a PHP wrapper.

This software was initially provided by OlyCash (olycash.com) to 
encourage software solutions for the unbanked.


CONTRIBUTING:
Feel free to submit merge requests for bugfixes and improvements. 


LICENSE:
This project code is provided free to use and reproduce under MIT license.


