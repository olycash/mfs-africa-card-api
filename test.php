<?php
/*********************************************************************
 * Test access to the MFS Africa Card API
 *********************************************************************/
require_once('Wrapper.php'); 

# The wrapper to use in this test
$testWrapper = new Wrapper;

echo "\n=============================\n";
echo "\nTesting encrypting..\n";

$payload = [
    "last4Digits"=> "1233",
    "mobilePhoneNumber"=> "232323322"
];
echo "Payload: ".json_encode($payload);
echo "\n\n";

$encryptedValue = $testWrapper->encrypt(['data'=>$payload ]);
echo "\nEncrypted Value:\n".$encryptedValue;
echo "\n=============================\n";


echo "\n\n\nTesting decrypting..\n";
$decryptedValue = $testWrapper->decrypt(['data'=>$encryptedValue ]);
echo "\nDecrypted Value:\n".json_encode($decryptedValue);
echo "\nPart [mobilePhoneNumber]:\n".$decryptedValue['mobilePhoneNumber'];
echo "\n=============================\n";



